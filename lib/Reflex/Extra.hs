{-# LANGUAGE RecursiveDo #-}

module Reflex.Extra where

import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO (..))
import Data.Align (align)
import Data.These (These (..))
import Data.These.Combinators (justHere, justThere)
import Data.Time (NominalDiffTime)
import Reflex

-- | Continuously patch the specified argument while receiving Right.
-- Fires and forgets upon receiving Left.
patching
  :: (MonadHold t m, MonadFix m, Reflex t, Patch a)
  => PatchTarget a
  -> Event t (Either () a)
  -> m (Event t (PatchTarget a))
patching initialValue newValues = do
  let f s x = (newState, out)
       where
        newState = case x of
          Right p -> apply p s
          Left () -> Nothing
        out = case x of
          Left () -> Just s
          _ -> Nothing
  (_, ev) <- mapAccumMaybeB f initialValue newValues
  return ev

-- | Trigger an IO action when the specified event fires.
at
  :: (PerformEvent t m, MonadIO (Performable m))
  => Event t a
  -> (a -> IO b)
  -> m (Event t b)
at ev m = performEvent $ liftIO . m <$> ev

-- | Filtered debounce; fires upon change but preserves the time window.
--
-- Upon receiving an event, create a time window. If another event is received
-- in that window, evaluates the predicate. If the predicate returns True, the
-- event is dismissed. Sends the original event at the end of the time window.
--
-- If the predicate returns False within the time window, an event with the
-- previous value is sent, and the new value replaces the previous one; when
-- the time window expires the new value is sent.
-- If an event is received as the window expires, the previous event is sent and
-- a new window is opened.
debounceFiltered
  :: ( TriggerEvent t m
     , PerformEvent t m
     , MonadIO (Performable m)
     , MonadHold t m
     , MonadFix m
     )
  => (a -> a -> Bool)
  -> NominalDiffTime
  -> Event t a
  -> m (Event t a)
debounceFiltered fn interval newValues = do
  let
    -- State evolution
    update = Just . Just
    clear = Just Nothing
    ignore = Nothing
    -- Event processing
    schedule = Just (That ())
    send = Just . This
    schedAndSend a = Just (These a ())
    f (Just s) = \case
      -- New value while debouncing.
      This a
        | s `fn` a -> (ignore, Nothing)
        | otherwise -> (update a, schedAndSend s)
      -- New value upon expiring debounce.
      These a _
        | s `fn` a -> (ignore, schedule)
        | otherwise -> (update a, schedAndSend s)
      -- Debounce expired.
      That _ -> (clear, send s)
    f Nothing = \case
      -- New value.
      This a -> (update a, schedule)
      -- Impossible: Missing state to send.
      These a b -> (update a, schedule)
      -- Impossible: Missing state to send.
      That _ -> (clear, Nothing)
  rec (_, ev) <- mapAccumMaybeB f Nothing (align newValues debounced)
      debounced <- debounce interval (fmapMaybe justThere ev)
  pure $ fmapMaybe justHere ev
