module Reflex.Network.Extra where

import Control.Monad.IO.Class (MonadIO (..))
import Reflex
import Reflex.Network (networkHold)

-- | Returns an Event that 'reconfigures' itself upon another event.
reconfiguringEvent
  :: ( MonadHold t m
     , NotReady t m
     , Adjustable t m
     , TriggerEvent t m
     , PerformEvent t m
     , MonadIO m
     , MonadIO (Performable m)
     )
  => Event t a
  -- ^ The event triggering reconfiguration.
  -> (a -> m (Event t b))
  -- ^ Function constructing a new event and cleanup function.
  -> m (Event t b)
reconfiguringEvent feed builder = do
  (ev, fire) <- newTriggerEvent
  performEvent (liftIO . fire . builder <$> feed)
  networkHold (pure never) ev >>= switchHold never . updated
