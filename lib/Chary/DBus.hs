{-# LANGUAGE TemplateHaskell #-}

module Chary.DBus where

import Control.Exception (catchJust)
import Control.Monad (forM_, guard)
import Control.Monad.Extra (whenJustM)
import DBus (Signal (signalBody), formatObjectPath, fromVariant)
import DBus.Client (ClientError (..), MatchRule (..), addMatch, matchAny)
import DBus.TH.EDSL
import Data.Functor ((<&>))
import Data.List (isPrefixOf)
import Data.Map (Map)
import Data.Maybe (fromMaybe)
import Data.String.Interpolate
import Data.Text (Text)
import Data.Time (UTCTime, getCurrentTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import System.IO.Silently (silence)

type SecretDetails = (ObjectPath, [Word8], [Word8], String)
type MapStrVariant = Map String Variant

interface'
  "org.freedesktop.secrets"
  Nothing
  "org.freedesktop.DBus.Properties"
  Nothing
  [ "Get" =:: ''String :-> ''String :-> Return ''Variant
  , "Set" =:: ''String :-> ''String :-> ''Variant :-> Return ''()
  ]

-- | Alternative to @get@ where the ObjectPath is not a String.
getProp :: Client -> ObjectPath -> String -> String -> IO (Maybe Variant)
getProp dbus (formatObjectPath -> op) = get dbus op

interface'
  "org.freedesktop.secrets"
  Nothing
  "org.freedesktop.Secret.Collection"
  Nothing
  [ "ItemChanged" =:: Return ''()
  , "CreateItem" =:: ''MapStrVariant :-> ''SecretDetails :-> ''Bool :-> Returns [''ObjectPath, ''ObjectPath]
  ]

interface
  "org.freedesktop.secrets"
  "/org/freedesktop/secrets"
  "org.freedesktop.Secret.Service"
  Nothing
  [ "OpenSession" =:: ''String :-> ''Variant :-> Returns [''Variant, ''ObjectPath]
  , "ReadAlias" =:: ''Text :-> Return ''ObjectPath
  ]

interface'
  "org.freedesktop.secrets"
  Nothing
  "org.freedesktop.Secret.Item"
  Nothing
  [ "Delete" =:: Return ''ObjectPath
  ]

data ItemEvent a
  = ItemChanged UTCTime a
  | ItemCreated UTCTime a
  | ItemDeleted UTCTime a
  deriving (Show, Functor)

extractEvent :: ItemEvent a -> (UTCTime, a)
extractEvent = \case
  ItemChanged t a -> (t, a)
  ItemCreated t a -> (t, a)
  ItemDeleted t a -> (t, a)

connectCollection :: Client -> ObjectPath -> (ItemEvent ObjectPath -> IO ()) -> IO ()
connectCollection dbus collection cb = do
  addMatch dbus (mkMatch "ItemChanged") $ onMatch $ \objectPath ->
    whenJustM (getSecretModified dbus objectPath) $ \modified ->
      cb (ItemChanged modified objectPath)
  addMatch dbus (mkMatch "ItemCreated") $ onMatch $ \objectPath ->
    whenJustM (getSecretModified dbus objectPath) $ \modified ->
      cb (ItemCreated modified objectPath)
  addMatch dbus (mkMatch "ItemDeleted") $ onMatch $ \objectPath -> do
    modified <- getCurrentTime
    cb (ItemDeleted modified objectPath)
  pure ()
 where
  mkMatch elem =
    matchAny
      { matchMember = Just elem
      , matchInterface = Just "org.freedesktop.Secret.Collection"
      , matchPath = Just collection
      }
  onMatch m (signalBody -> [v]) = forM_ (fromVariant v) m
  onMatch _ _ = pure ()

getSecretModified :: Client -> ObjectPath -> IO (Maybe UTCTime)
getSecretModified dbus secret = do
  r <-
    maskObjectNotExists $
      getProp dbus secret "org.freedesktop.Secret.Item" "Modified"
  pure $ r >>= fmap (posixSecondsToUTCTime . fromIntegral) . (fromVariant @Word64)

getSecretLabel :: Client -> ObjectPath -> IO (Maybe Text)
getSecretLabel dbus secret = do
  r <-
    maskObjectNotExists $
      getProp dbus secret "org.freedesktop.Secret.Item" "Label"
  pure $ r >>= fromVariant

getCollectionLabel :: Client -> ObjectPath -> IO (Maybe Text)
getCollectionLabel dbus collection = do
  r <-
    maskObjectNotExists $
      getProp dbus collection "org.freedesktop.Secret.Collection" "Label"
  pure $ r >>= fromVariant

findCollection :: Client -> Text -> IO (Maybe ObjectPath)
findCollection dbus label = do
  r <- getProp dbus "/org/freedesktop/secrets" "org.freedesktop.Secret.Service" "Collections"
  let collections = fromMaybe @[ObjectPath] [] $ r >>= fromVariant
      findLabel c = fbind (getCollectionLabel dbus c) $ \l -> do
        guard (l == label)
        pure c
  foldl mplus' (pure Nothing) (findLabel <$> collections)

findSecret :: Client -> ObjectPath -> Text -> IO (Maybe ObjectPath)
findSecret dbus collection label = do
  r <-
    maskObjectNotExists $
      getProp dbus collection "org.freedesktop.Secret.Collection" "Items"
  let collections = fromMaybe @[ObjectPath] [] $ r >>= fromVariant
      findLabel c = fbind (getSecretLabel dbus c) $ \l -> do
        guard (l == label)
        pure c
  foldl mplus' (pure Nothing) (findLabel <$> collections)

findSecretModificationDate :: Client -> Text -> Text -> IO (Maybe UTCTime)
findSecretModificationDate dbus collection label = do
  findCollection dbus collection >>= \case
    Nothing -> pure Nothing
    Just cPath ->
      findSecret dbus cPath label >>= \case
        Nothing -> pure Nothing
        Just secretPath -> getSecretModified dbus secretPath

listCollectionItems :: Client -> ObjectPath -> IO [ObjectPath]
listCollectionItems dbus collection = do
  r <-
    maskObjectNotExists $
      getProp dbus collection "org.freedesktop.Secret.Collection" "Items"
  pure $ fromMaybe [] (r >>= fromVariant)

-- | Apply a monadic bind under a functor.
fbind :: (Functor f, Monad m) => f (m a) -> (a -> m b) -> f (m b)
fbind f fn = (>>= fn) <$> f

-- | A monad plus variant considering the embedded Nothing as failure.
mplus' :: IO (Maybe a) -> IO (Maybe a) -> IO (Maybe a)
mplus' m1 m2 = m1 >>= maybe m2 (pure . Just)

-- | Masks DBus error "Object does not exist" and returns Nothing instead.
maskObjectNotExists :: IO (Maybe a) -> IO (Maybe a)
maskObjectNotExists m = do
  let errIsNotExists (clientErrorMessage -> msg) =
        "Call failed: Object does not exist" `isPrefixOf` msg
  -- DBus makes a lot of noise when it throws an error.
  silence $
    -- Querying a non-existent object yields an error.
    catchJust @ClientError (guard . errIsNotExists) m (const $ pure Nothing)
