{-# LANGUAGE DeriveAnyClass #-}

module Chary.Config.File where

import Data.Aeson
import Data.Aeson.Types
import Data.Map (Map)
import Data.String (IsString (..))
import Data.Text (Text)
import GHC.Generics

-- | Name of a portable storage
type PSName = Text

-- | Name of a local storage
type LSName = Text

data Config = Config
  { portable_storages :: Map PSName PortableStorage
  , local_storages :: Map LSName LocalStorage
  , files :: Map FilePath FileConfig
  }
  deriving (Show, Generic, FromJSON)

newtype PortableStorage = Pass PSPass
  deriving (Show)

data PSPass = PSPass
  { password_store_dir :: Maybe FilePath
  , subpath :: Maybe FilePath
  }
  deriving (Show, Generic, FromJSON)

newtype LocalStorage = SecretService LSSecretService
  deriving (Show)

data LSSecretService = LSSecretService
  { keyring :: Text
  , portable_storage :: Maybe PSName
  , prefix :: Maybe Text
  }
  deriving (Show, Generic, FromJSON)

type SecretPath = Text

data FileConfig = FileConfig
  { command :: Text
  , secrets :: [Map LSName SecretPath]
  }
  deriving (Show, Generic, FromJSON)

instance FromJSON PortableStorage where
  parseJSON obj@(Object v) = do
    backend <- v .: fromString "type"
    case backend of
      "password-store" -> Pass <$> parseJSON obj
      _ -> fail $ "parsing portable storage failed, unknown type " <> backend
  parseJSON invalid =
    prependFailure
      "parsing portable storage failed, "
      (typeMismatch "Object" invalid)

instance FromJSON LocalStorage where
  parseJSON obj@(Object v) = do
    backend <- v .: fromString "type"
    case backend of
      "dbus-secret-service" -> SecretService <$> parseJSON obj
      _ -> fail $ "parsing local storage failed, unknown type " <> backend
  parseJSON invalid =
    prependFailure
      "parsing local storage failed, "
      (typeMismatch "Object" invalid)
