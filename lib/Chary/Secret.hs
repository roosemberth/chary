{-# LANGUAGE DataKinds #-}

module Chary.Secret where

import Data.Text (Text)
import Data.Time (UTCTime)

-- | A 'path' that the storage can use to locate the secret.
type PathToSecret = FilePath

data SecretState
  = -- | Secret exists and was possibly modified since the last event.
    SecretExists
  | -- | Secret no longer exists. No further events will be received about it.
    SecretDeleted
  deriving (Eq, Show)

data SecretStorage = PasswordStore | DBusSecretService

data Secret (storage :: SecretStorage) = Secret
  { name :: Text
  -- ^ The name of the secret. Usually the path with the 'subpath' removed.
  , path :: PathToSecret
  }
  deriving (Eq, Show)

data SecretUpdate s = SecretUpdate
  { state :: SecretState
  , secret :: Secret s
  , lastModified :: Maybe UTCTime
  }
  deriving (Eq, Show)
