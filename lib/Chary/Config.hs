{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE RecordWildCards #-}

module Chary.Config
  ( Config (..)
  , PortableStorage (..)
  , PSPass (..)
  , LSName
  , PSName
  , LocalStorage (..)
  , LSSecretService (..)
  , fromFile
  ) where

import Chary.Config.File
  ( FileConfig
  , LSName
  , LSSecretService (..)
  , LocalStorage (..)
  , PSName
  )
import Control.Monad (forM)
import Control.Monad.Extra (unlessM)
import Data.Functor ((<&>))
import Data.Map (Map)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import System.Directory (doesDirectoryExist, makeAbsolute)
import System.Environment (lookupEnv)

import Chary.Config.File qualified as File

data Config = Config
  { pss :: Map PSName PortableStorage
  , lss :: Map LSName LocalStorage
  , files :: Map FilePath FileConfig
  }
  deriving (Show)

newtype PortableStorage = Pass PSPass
  deriving (Show)

data PSPass = PSPass
  { pass_directory :: FilePath
  -- ^ Absolute path to the 'pass' directory.
  , subpath :: FilePath
  -- ^ Subdirectory to watch within the 'pass' directory.
  }
  deriving (Show)

data PassDirectorySource = ConfigFile | EnvVar

fromFile :: File.Config -> IO Config
fromFile cfgfile = do
  pss <- forM cfgfile.portable_storages $ \(File.Pass cfg) -> do
    (src, passDir) <- case cfg.password_store_dir of
      Just dir -> pure (ConfigFile, dir)
      Nothing ->
        lookupEnv "PASSWORD_STORE_DIR" >>= \case
          Just dir -> pure (EnvVar, dir)
          Nothing ->
            fail
              "The password_store_dir key was not defined and the \
              \PASSWORD_STORE_DIR environment variable is not defined."
    pass_directory <- makeAbsolute passDir
    unlessM (doesDirectoryExist pass_directory) $ fail $ case src of
      ConfigFile ->
        "The directory specified in the configuration file does not exist"
      EnvVar -> "The directory specified in PASSWORD_STORE_DIR does not exist."
    let subpath = fromMaybe "" cfg.subpath
    pure (Pass PSPass{..})

  let lss = cfgfile.local_storages
      files = cfgfile.files
  pure Config{..}
