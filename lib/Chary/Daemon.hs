{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}

module Chary.Daemon where

import Chary.Config
  ( Config (..)
  , LSSecretService (..)
  , LocalStorage (..)
  , PortableStorage (..)
  )
import Chary.DBus (delete, findCollection, findSecret, findSecretModificationDate)
import Chary.Secret
import Chary.Watch.PassSecrets (watchPassSecrets)
import Chary.Watch.SecretService (watchSecrets)
import Control.Monad (void)
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO (..))
import DBus (formatObjectPath)
import DBus.Client (connectSession, disconnect)
import Data.List.NonEmpty qualified as NE
import Data.Map (Map)
import Data.Map qualified as M
import Data.String.Interpolate
import Data.Text (Text, pack)
import Reflex
import System.FilePath (splitPath)
import System.Process (readProcess)
import Text.URI
  ( Authority (..)
  , URI (..)
  , mkHost
  , mkPathPiece
  , mkScheme
  , renderStr
  )

type DaemonM t m =
  ( Adjustable t m
  , MonadFix m
  , MonadHold t m
  , MonadIO (Performable m)
  , MonadIO m
  , NotReady t m
  , PerformEvent t m
  , PostBuild t m
  , TriggerEvent t m
  )

runDaemon :: DaemonM t m => Config -> m (Event t ())
runDaemon Config{pss, lss, files} = do
  monitorsP <- mapM mkPortable pss
  monitorsL <- mapM mkLocal lss
  mapM_ (mkCircuit monitorsP) lss

  -- TODO:
  -- - Allocate networks for files
  -- - Connect files to ls
  pure never
 where
  mkPortable (Pass psp) = watchPassSecrets psp
  mkLocal (SecretService ss) = watchSecrets ss

mkCircuit
  :: DaemonM t m
  => Map Text (Event t (SecretUpdate 'PasswordStore))
  -> LocalStorage
  -> m ()
mkCircuit _ (SecretService LSSecretService{portable_storage = Nothing}) = pure ()
mkCircuit
  monitors
  (SecretService ss@LSSecretService{portable_storage = Just ps}) = do
    ev <- case M.lookup ps monitors of
      Nothing -> error [i|Portable storage #{ps} is not defined.|]
      Just ev -> pure ev
    performEvent $ ffor ev $ liftIO . handleSecretUpdate ss
    pure ()

handleSecretUpdate :: LSSecretService -> SecretUpdate 'PasswordStore -> IO ()
handleSecretUpdate LSSecretService{keyring} = \case
  SecretUpdate
    { state = SecretExists
    , secret = Secret{path, name}
    , lastModified = mLastModified
    } -> do
      dbus <- connectSession
      collection <-
        findCollection dbus keyring >>= \case
          Just c -> pure c
          Nothing -> undefined
      mTargetModified <- findSecretModificationDate dbus keyring (pack path)
      disconnect dbus
      case (mLastModified, mTargetModified) of
        -- The target is more recent than the source: Skip.
        (Just src, Just dst)
          | dst > src ->
              putStrLn [i|Skipped SS update: #{keyring}:#{name} is more recient.|]
        _ -> do
          deleteSSSecret keyring path
          createSSSecret keyring path
  SecretUpdate{state = SecretDeleted, secret = Secret{path}} ->
    deleteSSSecret keyring path

deleteSSSecret :: Text -> String -> IO ()
deleteSSSecret keyring path = do
  dbus <- connectSession
  collection <-
    findCollection dbus keyring >>= \case
      Just c -> pure c
      Nothing -> fail [i|Keyring #{keyring} could not be found, cannot delete #{path}.|]
  while
    (findSecret dbus collection $ pack path)
    $ delete dbus . formatObjectPath
  disconnect dbus
 where
  while f m =
    f >>= \case
      Nothing -> pure ()
      Just v -> m v >> while f m

createSSSecret :: Text -> String -> IO ()
createSSSecret keyring path = do
  let splitPath' p = filter (/= '/') <$> splitPath p
      uriFragment = Nothing
      uriQuery = []
  let mSrc = do
        scheme <- mkScheme "password-store"
        paths <- mapM mkPathPiece (pack <$> splitPath' path) >>= NE.nonEmpty
        let uriScheme = Just scheme
            uriPath = Just (False, paths)
        pure URI{uriAuthority = Left True, ..}
  let mDst = do
        scheme <- mkScheme "dbus-secret-service"
        paths <- mapM mkPathPiece (pack <$> splitPath' path) >>= NE.nonEmpty
        authHost <- mkHost keyring
        let uriScheme = Just scheme
            uriPath = Just (False, paths)
            auth = Authority{authUserInfo = Nothing, authHost, authPort = Nothing}
        pure URI{uriAuthority = Right auth, ..}
  void $ case (mSrc, mDst) of
    (Nothing, _) -> fail [i|Failed to compose src URI for secret #{path}|]
    (_, Nothing) -> fail [i|Failed to compose dst URI for secret #{path}|]
    (Just src, Just dst) -> do
      readProcess "chary-worker" ["copy", renderStr src, renderStr dst] ""
