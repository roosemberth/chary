{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}

module Chary.Watch.SecretService where

import Chary.Config (LSSecretService)
import Chary.Config.File (LSSecretService (..))
import Chary.DBus
import Chary.Secret
import Control.Monad (forM_, guard)
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO (..))
import DBus (ObjectPath)
import DBus.Client (Client, connectSession)
import Data.Functor ((<&>))
import Data.Map (Map, empty, singleton)
import Data.Map qualified as M
import Data.Maybe (catMaybes, fromMaybe)
import Data.String.Interpolate
import Data.Text (Text, isPrefixOf, stripPrefix, unpack)
import Reflex
import Reflex.Extra (at)

type DBusWatch t m =
  ( MonadFix m
  , MonadHold t m
  , MonadIO (Performable m)
  , MonadIO m
  , PerformEvent t m
  , TriggerEvent t m
  )

watchCollectionSecrets
  :: (DBusWatch t m) => Client -> ObjectPath -> m (Event t (ItemEvent Text))
watchCollectionSecrets dbus collection = do
  -- Remember the object labels: Needed when firing the 'deleted' event.
  let memoize s x = (apply @(PatchMap ObjectPath Text) x s, Nothing)
  (lutUpdate, fireLutUpdate) <- newTriggerEvent
  (lut, _) <- mapAccumMaybeB memoize empty lutUpdate

  (itemEv, fireItemEv) <- newTriggerEvent
  liftIO $ connectCollection dbus collection fireItemEv

  (ev, fireEv) <- newTriggerEvent
  performEvent $ ffor (attach lut itemEv) $ \(lut, e) -> do
    let (_, objectPath) = extractEvent e
    mNewLabel <- liftIO $ getSecretLabel dbus objectPath
    liftIO $ fireLutUpdate (PatchMap $ singleton objectPath mNewLabel)
    liftIO $ sendWatchEvents fireEv e (M.lookup objectPath lut) mNewLabel
  -- Announce existing elements.
  liftIO $ do
    let toEvent item = fmap (`ItemCreated` item) <$> getSecretModified dbus item
    events <- listCollectionItems dbus collection >>= mapM toEvent
    mapM_ fireItemEv (catMaybes events)
  pure ev

watchSecrets
  :: DBusWatch t m
  => LSSecretService
  -> m (Event t (SecretUpdate DBusSecretService))
watchSecrets LSSecretService{keyring, prefix = fromMaybe "" -> prefix} = do
  dbus <- liftIO connectSession
  collection <-
    liftIO $
      findCollection dbus keyring >>= \case
        Nothing -> fail $ "Could not find collection " ++ unpack keyring
        Just a -> pure a
  fmapMaybe toSecretUpdate <$> watchCollectionSecrets dbus collection
 where
  toSecretUpdate secret = do
    guard (isPertinent . snd $ extractEvent secret)
    Just $ case secret of
      ItemChanged ts label -> toUpdate SecretExists ts label
      ItemCreated ts label -> toUpdate SecretExists ts label
      ItemDeleted ts label -> toUpdate SecretDeleted ts label
  isPertinent label = prefix `isPrefixOf` label
  toSecret label = Secret{..}
   where
    name = fromMaybe label $ stripPrefix prefix label
    path = unpack label
  toUpdate state ts label = SecretUpdate state (toSecret label) (Just ts)

-- | Given an ItemEvent, its old name and its new name, decides and sends
-- events under watch.
sendWatchEvents
  :: (ItemEvent Text -> IO ())
  -> ItemEvent ObjectPath
  -> Maybe Text
  -> Maybe Text
  -> IO ()
sendWatchEvents sender ev mOldLabel mNewLabel = case (mOldLabel, mNewLabel <$ ev) of
  (Just l1, ItemChanged t (Just l2)) -> do
    sender (ItemDeleted t l1)
    sender (ItemCreated t l2)
  (Just l, ItemChanged t Nothing) -> sender (ItemDeleted t l)
  (Just l, ItemDeleted t Nothing) -> sender (ItemDeleted t l)
  (_, ItemCreated t (Just l)) -> sender (ItemCreated t l)
  (_, ItemCreated _ Nothing) ->
    putStrLn [i|#{ev} event, but it's gone now.|]
  (Nothing, ItemChanged t (Just l)) -> sender (ItemCreated t l)
  (Nothing, ItemChanged _ Nothing) ->
    putStrLn [i|#{ev} event, but it's gone now.|]
  (Nothing, ItemDeleted _ Nothing) ->
    putStrLn [i|#{ev} event, but it's unknown.|]
  (Nothing, ItemDeleted _ (Just l)) ->
    putStrLn [i|#{ev} event, but it's still there: #{l}.|]
