{-# LANGUAGE DataKinds #-}

module Chary.Watch.PassSecrets where

import Chary.Config (PSPass (..))
import Chary.Secret
import Chary.Watch.FileTree
  ( FileEvent (..)
  , FileTreeWatch
  , extractPath
  , watchFileTree
  )
import Control.Monad (guard)
import Data.List (isPrefixOf, stripPrefix)
import Data.String (IsString (..))
import Reflex
import Reflex.Extra (debounceFiltered)
import System.FilePath (dropExtension, makeRelative, takeDirectory, takeExtension)

watchPassSecrets
  :: FileTreeWatch t m => PSPass -> m (Event t (SecretUpdate PasswordStore))
watchPassSecrets PSPass{pass_directory, subpath} =
  watchFileTree pass_directory
    >>= debounceFiltered (==) 0.5 . fmapMaybe toSecretUpdate
 where
  toSecretUpdate secret = do
    guard (isPertinent $ extractPath secret)
    case secret of
      FileAdded ts fp -> Just (toUpdate SecretExists ts fp)
      FileModified ts fp -> Just (toUpdate SecretExists ts fp)
      FileRemoved ts fp -> Just (toUpdate SecretDeleted ts fp)
  isPertinent fp =
    takeExtension fp == ".gpg"
      && isPrefixOf subpath (takeDirectory fp)
  toSecret fp = Secret{..}
   where
    name = fromString (makeRelative subpath (dropExtension fp))
    path = fromString (dropExtension fp)
  toUpdate state ts fp = SecretUpdate state (toSecret fp) (Just ts)
