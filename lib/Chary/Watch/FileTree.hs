{-# LANGUAGE QuasiQuotes #-}

module Chary.Watch.FileTree where

import Chary.Config
import Control.Exception (SomeException, catch)
import Control.Monad (forM, forM_)
import Control.Monad.Extra (unlessM)
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class
import Data.String (IsString (..))
import Data.String.Interpolate
import Data.Text (Text, unpack)
import Data.Text.Encoding (decodeUtf8)
import Data.Time (UTCTime)
import Data.Time.Clock
import Data.Tree (Tree (..))
import Data.TreePatch (TreePatch (..), UpdateResult (..))
import Reflex
import Reflex.Extra (at, patching)
import Reflex.Network.Extra (reconfiguringEvent)
import System.Directory
  ( doesDirectoryExist
  , doesFileExist
  , getDirectoryContents
  , getModificationTime
  )
import System.FilePath (takeFileName, (</>))
import System.INotify
  ( EventVariety (..)
  , INotify
  , addWatch
  , initINotify
  , removeWatch
  )
import System.INotify qualified (Event (..))

-- | The network requirements for watching files and subdirectories.
type FileTreeWatch t m =
  ( Adjustable t m
  , MonadHold t m
  , MonadIO (Performable m)
  , MonadIO m
  , MonadFix m
  , NotReady t m
  , PerformEvent t m
  , PostBuild t m
  , TriggerEvent t m
  )

type FileTree = Tree FilePath
type FileTreePatch = TreePatch FilePath

data FileEvent a
  = FileAdded UTCTime a
  | FileModified UTCTime a
  | FileRemoved UTCTime a
  deriving (Show, Functor)

extractPath :: FileEvent FilePath -> FilePath
extractPath (FileAdded _ fp) = fp
extractPath (FileModified _ fp) = fp
extractPath (FileRemoved _ fp) = fp

-- | Adds a directory prefix to a @FileEvent@.
addPrefix :: FilePath -> FileEvent FilePath -> FileEvent FilePath
addPrefix prefix = fmap (prefix </>)

-- | Generates a patch for the specified event if necessary.
toPatch
  :: FilePath
  -- ^ Name of the parent, in case the parent did not exist.
  -> FileEvent FilePath
  -> FileTreePatch
toPatch parent fe = TreePatch $ case fe of
  (FileAdded _ fp) -> \case
    Nothing -> Updated (Node parent [Node fp []])
    Just (Node n ts)
      | any (isFp fp) ts -> Unchanged
      | otherwise -> Updated (Node n $ Node fp [] : ts)
  (FileRemoved _ fp) -> \case
    Nothing -> Unchanged
    Just (Node n ts)
      | any (isFp fp) ts -> Updated . Node n $ filter (not . isFp fp) ts
      | otherwise -> Unchanged
  (FileModified _ _) -> const Unchanged
 where
  isFp fp (Node n _) = n == fp

data DirEvent = DirAdded FilePath | DirRemoved FilePath
  deriving (Show)

-- | Whether the event indicates the directory being watched was deleted.
selfWasDeleted :: System.INotify.Event -> Bool
selfWasDeleted = \case
  System.INotify.DeletedSelf{} -> True
  System.INotify.MovedSelf{} -> True
  System.INotify.Unmounted{} -> True
  _ -> False

-- | Extracts the path an event pertains to an INotify event.
subEventPath :: System.INotify.Event -> Maybe FilePath
subEventPath = \case
  System.INotify.MovedOut{filePath} -> decode' filePath
  System.INotify.MovedIn{filePath} -> decode' filePath
  System.INotify.Created{filePath} -> decode' filePath
  System.INotify.Deleted{filePath} -> decode' filePath
  System.INotify.Modified{maybeFilePath} -> decode <$> maybeFilePath
  System.INotify.Closed{maybeFilePath} -> decode <$> maybeFilePath
  _ -> Nothing
 where
  decode = takeFileName . unpack . decodeUtf8
  decode' = Just . decode

-- | Tests whether the specified INotify event pertains a directory.
subEventIsDirectory :: System.INotify.Event -> Bool
subEventIsDirectory = \case
  System.INotify.MovedOut{isDirectory} -> isDirectory
  System.INotify.MovedIn{isDirectory} -> isDirectory
  System.INotify.Created{isDirectory} -> isDirectory
  System.INotify.Deleted{isDirectory} -> isDirectory
  _ -> False

-- | Decodes an INotify event about a directory.
-- This function blindly assumes the event pertains a directory!
decodeSubDirEvent :: System.INotify.Event -> Maybe DirEvent
decodeSubDirEvent = \case
  System.INotify.MovedOut{filePath = decode -> fp} -> Just $ DirRemoved fp
  System.INotify.MovedIn{filePath = decode -> fp} -> Just $ DirAdded fp
  System.INotify.Created{filePath = decode -> fp} -> Just $ DirAdded fp
  System.INotify.Deleted{filePath = decode -> fp} -> Just $ DirRemoved fp
  _ -> Nothing
 where
  decode = takeFileName . unpack . decodeUtf8

-- | Decodes an INotify event about a file.
-- This function blindly assumes the event pertains a regular file!
decodeSubFileEvent :: UTCTime -> System.INotify.Event -> Maybe (FileEvent FilePath)
decodeSubFileEvent lastModified e = case e of
  System.INotify.MovedOut{} -> FileRemoved lastModified <$> fp
  System.INotify.MovedIn{} -> FileAdded lastModified <$> fp
  System.INotify.Created{} -> FileAdded lastModified <$> fp
  System.INotify.Deleted{} -> FileRemoved lastModified <$> fp
  System.INotify.Modified{} -> FileModified lastModified <$> fp
  System.INotify.Closed{} -> FileModified lastModified <$> fp
  _ -> Nothing
 where
  fp = subEventPath e

-- | Watches files under the specified directory and its subdirectories.
-- You may want to use @watchFileTree@ instead.
watchFileTree'
  :: FileTreeWatch t m
  => INotify
  -> FilePath
  -> m (Event t (FileEvent FilePath))
watchFileTree' inotify path = do
  unlessM
    (liftIO $ doesDirectoryExist path)
    (liftIO $ fail [i|Cannot watch directory: #{path} is not a directory.|])
  let evs = [MoveSelf, DeleteSelf, Modify, MoveIn, MoveOut, Create, Delete]
      cwd = takeFileName path

  (inotifyEv, fireInotifyEv) <- newTriggerEvent
  watchToken <- liftIO $ addWatch inotify evs (fromString path) fireInotifyEv

  (patchEv, firePatchEv) <- newTriggerEvent
  (fileEv, fireFileEv) <- newTriggerEvent
  (dirEv, fireDirEv) <- newTriggerEvent

  at inotifyEv $ \ev -> case () of
    _ | subEventIsDirectory ev -> forM_ (decodeSubDirEvent ev) $ \case
      DirRemoved _ -> pure () -- No need to reconfigure the network.
      DirAdded e -> fireDirEv e
    _ -> forM_ (subEventPath ev) $ \fp -> do
      lastModify <- getModificationTime fp
      forM_ (decodeSubFileEvent lastModify ev) fireFileEv

  -- If a directory was moved, batching gives us a change to prevent a network
  -- from being calculated and immediately superseeded.
  batchedDirEvs <- batchOccurrences 0.1 dirEv
  subEvs <- reconfiguringEvent batchedDirEvs $ \dirs -> do
    (subdirEv, fireSubdirEv) <- newTriggerEvent
    forM_ dirs $ \d -> do
      let withDirName = addPrefix $ takeFileName d
      evs <- fmap withDirName <$> watchFileTree' inotify (path </> d)
      at evs fireSubdirEv
    pure subdirEv
  at subEvs fireFileEv

  liftIO $
    getDirectoryContents path >>= \ff -> forM ff $ \fname -> do
      let filename = takeFileName fname
          fullPath = path </> filename
      isFile <- doesFileExist fullPath
      isDir <- doesDirectoryExist fullPath
      lastModify <- getModificationTime fullPath

      case (isFile, isDir) of
        (True, _) -> fireFileEv (FileAdded lastModify filename)
        (_, True) | filename `notElem` [".", ".."] -> fireDirEv filename
        (_, _) -> pure ()

  -- This event triggers when the current directory disappears.
  shouldDeleteSecrets <- patching (Just $ Node cwd []) patchEv
  at shouldDeleteSecrets $ \tree -> do
    let files = maybe [] subForest tree >>= flattenFileTree
    currentTime <- getCurrentTime
    mapM_ fireFileEv (FileRemoved currentTime <$> files)
    -- Depending on how we terminated, the watch may have already been removed.
    catch @SomeException
      (removeWatch watchToken)
      (const $ pure ())

  -- Fire deletion of the secrets we registered.
  at (ffilter selfWasDeleted inotifyEv) $ \_ -> firePatchEv $ Left ()

  -- Memorize the secrets we register for when the directory is deleted.
  at fileEv $ firePatchEv . Right . toPatch cwd
  pure fileEv

-- | Flattens a @FileTree@ by prefixing parent directories.
flattenFileTree :: FileTree -> [FilePath]
flattenFileTree (Node n []) = [n]
flattenFileTree (Node n ts) = fmap (n </>) (ts >>= flattenFileTree)

-- | Watches files under the specified directory and its subdirectories.
watchFileTree :: FileTreeWatch t m => FilePath -> m (Event t (FileEvent FilePath))
watchFileTree f = liftIO initINotify >>= flip watchFileTree' f
