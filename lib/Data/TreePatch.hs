module Data.TreePatch where

import Control.Monad.Free (Free (..))
import Control.Monad.Writer (Writer, runWriter)
import Data.Functor ((<&>))
import Data.Patch (Patch (..))
import Data.Tree (Tree (..))

data UpdateResult a = Deleted | Unchanged | Updated a
  deriving (Functor, Show)

-- | Potentially update a possibly non-existent tree.
-- If the function return Nothing, the update is discarded.
newtype TreePatch a = TreePatch (Maybe (Tree a) -> UpdateResult (Tree a))

instance Eq a => Patch (TreePatch a) where
  type PatchTarget (TreePatch a) = Maybe (Tree a)

  apply :: TreePatch a -> Maybe (Tree a) -> Maybe (Maybe (Tree a))
  apply (TreePatch act) t = case act t of
    Unchanged -> Nothing
    Deleted -> Just Nothing
    Updated t -> Just (Just t)

-- | Potentially update a possibly non-existent subtree.
-- If the function return Nothing, the update is discarded.
-- The path where the update should be applied is encoded in the Free structure:
-- The tag provided by the writer leads the descent.
-- If the subtree does not exist, but the Patch returns 'Just', it is created.
type SubTreePatch a = Free (Writer a) (TreePatch a)

instance Eq a => Patch (SubTreePatch a) where
  type PatchTarget (SubTreePatch a) = Maybe (Tree a)

  apply :: SubTreePatch a -> Maybe (Tree a) -> Maybe (Maybe (Tree a))
  apply (Pure pt) t = apply pt t
  apply (Free (runWriter -> (pst, tag))) Nothing = fmap toTree <$> apply pst Nothing
   where
    toTree t = Node tag [t]
  apply (Free (runWriter -> (pst, tag))) (Just (Node n ts)) = fmap (Node n) <$> fn ts
   where
    fn :: [Tree a] -> Maybe (Maybe [Tree a])
    fn [] = fmap (: []) <$> apply pst Nothing -- We didn't find the target.
    fn (t@(Node n ns) : ts)
      | n == tag = fmap (: ts) <$> apply pst (Just t)
      | otherwise = fmap (t :) <$> fn ts
