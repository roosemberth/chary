# chary

> 🚧 Project under construction 🚧. 🐉

This project provides a solution for user secret enrollment and file generation
based on user secrets. It uses [freedesktop's secret-service][secret-service]
to store the secrets, so applications can request them via dbus.

[secret-service]: https://specifications.freedesktop.org/secret-service/latest/

The practice of booting systems with ephemeral root storage is commonly referred
to as _impermanence_. The idea behind it is to create systems where the root
directory gets wiped every reboot. Operating systems such as [NixOS][] allow
a declarative system configuration, thus enabling systems with stateless
services or services where such state is explicitly declared by pre-generating
all of the system configuration. One of the tangible benefits of this is
preventing code describing a system drifting from the system state over time.

[NixOS]: https://nixos.org/

While this idea works great for service configuration, it's rather cumbersome
for secret management (such as passwords), in particular user-related secrets.
Traditional secret-management solutions expose secrets when the system boots,
which brings unnecessary exposition. Moreover, it's particularly unsatisfying
since changing a user password shouldn't require the system to be regenerated.

## Description

This tool is an effort to bridge the gap between static system configuration and
convenient user secret management. User secrets can be seen as time-varying
values with infrequent but unpredictable updates. Each secret has their own
lifetime and are for the most part agnostic of the technology that consumes
them (e.g. your email password does not depend on your email client).

By using [freedesktop's secret-service][secret-service] API, this project
integrates on a mature ecosystem. In particular, implementations like
[GNOME Keyring][] allows configuring a strategy requiring the user to provide a
password every time the computer is unlocked; this can be automated if desired
by using [GNOME Keyring's PAM plugin][pam-plugin] to unlock the user keyring
when the user logs-in.

[GNOME Keyring]: https://wiki.archlinux.org/title/GNOME/Keyring
[pam-plugin]: https://wiki.gnome.org/Projects/GnomeKeyring/Pam

Secrets are enrolled from a portable storage (such as [password-store][], but
other backends such as 1password may be integrated later).

[password-store]: https://www.passwordstore.org/

### Example workflow

```mermaid
graph LR
    PS[Pass]
    LS[GNOME Keyring]
    files[Files]
    cmd[Commands]

    PS -->|enroll| LS
    LS -->|generate| files
    LS ---|query via DBus| cmd
```

## Goals

- Dynamic enrollment of user secrets from portable storage.
- Dynamic generation of configuration from user secrets.

### Non goals

- Deployment of secrets.
- Secret storage.

## Usage

This project provides two binaries:

- `chary` is a daemon that will listen and react to changes in the system.
  It will perform deletion operations, but delegate any operations requiring
  access to secrets to the worker.
- `chary-worker` is called by the daemon for any operationg requiring access to
  the secret: Since the daemon is a long-lived process, the worker lifetime is
  much shorter thus preventing secrets from lingering in memory. Mind that at
  the moment, the worker does not `mlock(3)` the pages where the secret is read.

To run simply run `chary /path/to/config`. An example configuration file is
provided in this repository.

### Quicks & TODO

- Upstream events always trigger a secret deletion + secret creation downstream.
- File generation is not implemented.

## References

Impermanence:

- https://grahamc.com/blog/erase-your-darlings
- https://www.reddit.com/r/archlinux/comments/ljjbzs/erase_system_at_every_boot/
- https://elis.nu/blog/2020/06/nixos-tmpfs-as-home/

## Screenshots

Running the daemon with the [example configuration file (e169428d)][sample-cfg]
yields the following result:

[sample-cfg]: https://gitlab.com/roosemberth/chary/-/blob/e169428d9cfb914d68750d0f4531fb8260b240ff/examples/config.yml

![GNOME Keyring secrets derived from pass](img/example-screenshot.png)
