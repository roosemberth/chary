{-# LANGUAGE QuasiQuotes #-}

module Main where

import Chary.Config (fromFile)
import Data.String.Interpolate
import Data.Yaml (decodeFileThrow)
import Options.Applicative
import Reflex.Host.Headless (runHeadlessApp)
import Chary.Daemon (runDaemon)

main :: IO ()
main = execParser (info (opts <**> helper) mods) >>= run
 where
  opts = argument str (metavar "CONFIG_FILE")
  mods =
    fullDesc
      <> header "Secret enrollment daemon."
      <> progDesc
        [i|Chary is a tool for synchronizing user secrets focused at solving
        issues in setups with ephemeral root filesystems. Chary monitors secret
        storage facilities, pushing changes forward to either other secret
        storage or triggering actions.|]

run :: FilePath -> IO ()
run configFile = do
  config <- fromFile =<< decodeFileThrow configFile
  runHeadlessApp (runDaemon config)
