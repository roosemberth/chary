{-# LANGUAGE QuasiQuotes #-}

module Main where

import Chary.DBus (createItem, findCollection, openSession)
import Chary.Secret (PathToSecret)
import DBus (IsVariant (toVariant), formatObjectPath)
import DBus.Client (connectSession)
import Data.List.NonEmpty qualified as NE
import Data.Map (Map)
import Data.Map qualified as M
import Data.String.Interpolate
import Data.Text (Text, pack, unpack)
import Options.Applicative
import System.FilePath (joinPath)
import System.Process (readProcess)
import Text.URI (Authority (..), URI (..), mkURI, unRText)

data CopyCmd = CopyCmd
  { source :: URI
  , dest :: URI
  }
  deriving (Show)

data OpenCmd = OpenCmd
  { envvars :: Map Text URI
  , cmdLine :: [Text]
  }
  deriving (Show)

data Command = Copy CopyCmd | Open OpenCmd
  deriving (Show)

type Opts = Command

copyCommand :: Parser CopyCmd
copyCommand = CopyCmd <$> argument uriRead srcMods <*> argument uriRead dstMods
 where
  srcMods = metavar "URI" <> help "Where to retrieve the secret from."
  dstMods = metavar "URI" <> help "Where to copy the secret to."

uriRead :: ReadM URI
uriRead = maybeReader (mkURI . pack)

openCommand :: Parser OpenCmd
openCommand =
  OpenCmd
    <$> (fmap M.fromList . many) envNameUriPair
    <*> some (argument str (metavar "command..."))
 where
  envNameUriPair :: Parser (Text, URI)
  envNameUriPair = (,) <$> strOption envMods <*> option uriRead uriMods
  envMods = short 'e' <> metavar "envvar" <> help "Name of the envvar where to put the secret."
  uriMods = short 'v' <> metavar "URI" <> help "URI where to retrieve the secret from"

opts :: Parser Opts
opts =
  subparser $
    idm
      <> command "copy" (info (Copy <$> copyCommand <**> helper) copyMods)
      <> command "open" (info (Open <$> openCommand <**> helper) openMods)
 where
  copyMods = progDesc "Copies secrets between two backends."
  openMods = progDesc "Unlock the specified secrets and run the specified command."

main :: IO ()
main = execParser (info (opts <**> helper) mods) >>= run
 where
  mods = fullDesc <> header "Executes actions delegated by the chary daemon."

run :: Opts -> IO ()
run (Copy (CopyCmd src dst)) = runCopyCmd src dst
run (Open _) = fail "Open command not implemented yet."

runCopyCmd :: URI -> URI -> IO ()
runCopyCmd
  URI{uriScheme = Just (unRText -> srcProto), uriPath = srcUriPath}
  URI
    { uriScheme = Just (unRText -> dstProto)
    , uriPath = dstUriPath
    , uriAuthority = Right (Authority{authHost = dstHost})
    }
    | srcProto == "password-store" && dstProto == "dbus-secret-service" = do
        srcPath <- case srcUriPath of
          Just (_, srcPath) ->
            pure $ joinPath (unpack . unRText <$> NE.toList srcPath)
          _ -> fail "The specified source URI is not valid."
        dstPath <- case dstUriPath of
          Just (_, dstPath) ->
            pure $ joinPath (unpack . unRText <$> NE.toList dstPath)
          _ -> fail "The specified destination URI is not valid."
        copyPassToSS srcPath (unRText dstHost) dstPath
runCopyCmd _ _ = fail "I don't know how to copy between these protocols."

copyPassToSS :: PathToSecret -> Text -> PathToSecret -> IO ()
copyPassToSS passPath keyring ssPath = do
  dbus <- connectSession
  collection <-
    findCollection dbus keyring >>= \case
      Nothing -> fail [i|Could not find keyring with name '#{keyring}'|]
      Just c -> pure c
  (_, session) <-
    openSession dbus "plain" (toVariant @String "") >>= \case
      Nothing -> fail "Could not open session"
      Just c -> pure c

  secret <- readProcess "pass" ["show", passPath] ""
  r <-
    createItem
      dbus
      (formatObjectPath collection)
      (M.singleton "org.freedesktop.Secret.Item.Label" (toVariant ssPath))
      (session, [], [], "text/plain")
      True
  case r of
    Nothing -> fail "Failed to create DBus secret."
    Just c -> putStrLn [i|Secret created at path #{c}|]
