{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }: with nixpkgs.lib; let
    systems = [ "x86_64-linux" "i686-linux" ];
    forAllSystems = f: genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
    }));
    haskellDeps = drv: concatLists (attrValues drv.getCabalDeps);
    mkHs = pkgs: pkgs.haskellPackages.override (o: with pkgs.haskell.lib; {
      overrides = pkgs.lib.composeExtensions
        (o.overrides or (_: _: {}))
        (final: prev: {
          dbus-th = appendPatches prev.dbus-th [
            (pkgs.fetchpatch {
              name = "Support-methods-returning-multiple-values.patch";
              url = "https://github.com/portnov/dbus-th/pull/1/commits/99bae767bf2607c0dbc0fc0afcabeb3815690a93.patch";
              hash = "sha256-UMUur886r/Inpd5+kms5E2UQipqILPFXO3n9jwgVgdU=";
            })
            (pkgs.fetchpatch {
              name = "Fix-codegen-for-functions-returning-unit.patch";
              url = "https://github.com/portnov/dbus-th/pull/2/commits/63f284903731fed59661603bcddf925ed3a99dc5.patch";
              hash = "sha256-D7Sn2xlrxvE7vtBgwpCrxq5EmGYbfwUEOLae6eBAXPY=";
            })
          ];
        });
    });
  in {
    overlay = final: prev: with final; {
      haskellPackages = mkHs prev;
      chary = haskellPackages.callCabal2nix "chary" ./. {};
      default = final.chary;
    };

    devShells = forAllSystems (pkgs: with pkgs; {
      default = mkShell {
        inputsFrom = [ chary ];
        packages = [
          (haskellPackages.ghcWithPackages (ps: haskellDeps chary))
          haskellPackages.cabal-install
          haskellPackages.haskell-language-server
          haskellPackages.hpack
          haskellPackages.fourmolu
        ];
      };
    });

    # Export all packages from the overlay attribute.
    packages = forAllSystems (pkgs: with pkgs.lib;
      getAttrs (attrNames (self.overlay {} {})) pkgs
    );
  };
}
